const Category = require("../schema/categoryschema");


const getallcategory = async(req, res) => {
   const categories = await Category.find({});
    res.status(200).json(categories)
  }
 
const getcategorybyid = async(req, res) => {
  try{
    const categories = await Category.findById(req.params.categoryID).exec();
    res.status(200).json(categories)
  }
  catch(error){
    res.status(404).send("category not found")
  }
  
  }  

const addnewcategory = async (req, res) => {
  const categoriesdata = req.body
  const categories = new Category(categoriesdata)
  await categories.save();
  res.json(categories)
  }

const updatecategory =(req, res) => {
    res.send('Not written')
  }  

const deletecategory = (req, res) => {
    res.send('Not written')
  }
  
  module.exports = {getallcategory,
    getcategorybyid,
    addnewcategory,
    updatecategory,
    deletecategory
  }